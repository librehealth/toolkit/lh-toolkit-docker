# Pre-requisites for installing Docker on Mac

1. Docker requires OS X El Capitan 10.11 or newer macOS release running on a 2010 or newer Mac.
2. At least 4GB of RAM in the system.

#### Installing Docker on Mac

1. Download Docker for mac from the url : https://download.docker.com/mac/stable/Docker.dmg

2. Click on the Docker.dmg file you have just downloaded to mount the image and open the installer.
![](images/macOS/step1.png)

3. Drag and drop the Docker.app file into the Applications directory. Once you have dragged the Docker icon to your Applications folder, double-click on it and you will be asked whether you want to open the application you have downloaded. Saying yes will open the Docker installer:
![](images/macOS/step3.png)

4. Click next on the installer screen and follow the instructions in the installer.
![](images/macOS/step5.png)

5. Once installed you would see an Icon in top-right icon bar.
![](images/macOS/step6.png)


6. Click the whale icon in the icon bar to check “About Docker” page.


8. You can also open a terminal and run below command to get information on the installed docker.

````bash
$ docker version
````
9. To check if Docker machine and Docker Compose are installed as well, use the command below.

````bash
$ docker-compose version
$ docker-machine version
````
10. You can also run a Dockerized web server to make sure everything works:

To run containers pull the container image:
```
docker pull registry.gitlab.com/librehealth/toolkit/lh-toolkit-docker:latest
```
You should see the similar console output:

![](images/macOS/docker_pull.png)

Navigate to the directory where you cloned this project. Depending on how you want to interact with the container, run it in foreground or as a daemon.
To run the container in the foreground:
```
docker-compose -f docker-compose.dev.yml up
```

![](images/macOS/docker_up.png)

Now run this in your Browser at  localhost:8080/lh-toolkit

![](images/macOS/docker_run.png)
To run the container in the background:
```
docker-compose -f docker-compose.dev.yml up -d
```



## Multiple Docker Versions
Docker for Mac replaces docker and docker-compose with its own versions; if you already have Docker Toolbox on your Mac, Docker for Mac still replaces the binaries. You want the Docker client and Engine to match versions; mismatches can cause problems where the client and host cannot communicate. If you already have Docker Toolbox, and then you install Docker for Mac, you may get a newer version of the Docker client.
